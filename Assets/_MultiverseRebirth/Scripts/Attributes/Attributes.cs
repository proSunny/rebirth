﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ParameterName = ParametersList.Parameter;

public class Attributes {

    public List<Attribute> attributes;
    
    // list servises [Items, Effects, Quests, Achievements, Labyrinth]

    private Dictionary<ParameterName, float> exactParameters;
    private Dictionary<ParameterName, float> percentParameters;

    /*
    public Dictionary<Parameter, float> ToParams()
    {        
        foreach (Attribute attribute in attributes)
        {            
            if (attribute.valueType == Attribute.ValueType.exact)
            {
                // working with attributes that have exact ValueType
                float attributeValue;
                if (!exactParameters.TryGetValue(attribute.Parameter, out attributeValue))
                {
                    exactParameters.Add(attribute.Parameter, attribute.Value);                    
                }
                else
                {
                    exactParameters[attribute.Parameter] += attribute.Value;
                }
            }
            if (attribute.valueType == Attribute.ValueType.percent)
            {
                // working with attributes that have percent ValueType                
                float attributeValue;
                if (!percentParameters.TryGetValue(attribute.Parameter, out attributeValue))
                {
                    percentParameters.Add(attribute.Parameter, attribute.Value);
                }
                else
                {
                    percentParameters[attribute.Parameter] += attribute.Value;
                }
            }
        }

        // applying percentParameters to exactParameters
        foreach (KeyValuePair<Parameter, float> entry in exactParameters)
        {            
            float attributeValue;
            if (percentParameters.TryGetValue(entry.Key, out attributeValue))
            {
                exactParameters[entry.Key] = entry.Value * (1 + attributeValue);
            }
        }

        return exactParameters;        
    }
    */

    public void UpdateAttributes()
    {

    }

}