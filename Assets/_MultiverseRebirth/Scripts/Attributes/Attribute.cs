﻿[System.Serializable]
public class Attribute : IAttribute
{
    /* all variables here are fields and not parameters. Parameters are not serializable */
    public ParametersList.Parameter parameter;
    public float value;
    public AttributeType.ValueMod valueMod;
}
