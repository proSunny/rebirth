﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttributeList
{
    public enum Attributes
    {
        None,
        Strenght,
        Dexterity,
        Intellect,
        Willpower,
        Speed,
        Vitality
    };
}
