using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public abstract class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	protected GameObject draggingIcon;

    public abstract void OnBeforeDrag();    

	public virtual void OnBeginDrag(PointerEventData eventData)
	{
		var canvas = FindInParents<Canvas>(gameObject);
		if (canvas == null)
			return;

        OnBeforeDrag();

        // We have clicked something that can be dragged.
        // What we want to do is create an icon for this.
        draggingIcon = new GameObject("draggingIcon");

		draggingIcon.transform.SetParent (canvas.transform, false);
		draggingIcon.transform.SetAsLastSibling();
		
		var image = draggingIcon.AddComponent<Image>();
		// The icon will be under the cursor.
		// We want it to be ignored by the event system.
		var group = draggingIcon.AddComponent<CanvasGroup>();
		group.blocksRaycasts = false;

		image.sprite = GetComponent<Image>().sprite;
        image.color = new Color(1f, 1f, 1f, 0.7f);
        image.SetNativeSize();
		
		SetDraggedPosition(eventData);
	}

	public virtual void OnDrag(PointerEventData eventData)
	{
		if (draggingIcon != null)
			SetDraggedPosition(eventData);
	}

	private void SetDraggedPosition(PointerEventData eventData)
	{
		var rt = draggingIcon.GetComponent<RectTransform>();
		// rt.position = Input.mousePosition;

        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
        rt.position = curPosition;
	}

	public virtual void OnEndDrag(PointerEventData eventData)
	{
        if (draggingIcon != null)
            Destroy(draggingIcon);

        draggingIcon = null;
    }

	static public T FindInParents<T>(GameObject go) where T : Component
	{
		if (go == null) return null;
		var comp = go.GetComponent<T>();

		if (comp != null)
			return comp;
		
		var t = go.transform.parent;
		while (t != null && comp == null)
		{
			comp = t.gameObject.GetComponent<T>();
			t = t.parent;
		}
		return comp;
	}
}
