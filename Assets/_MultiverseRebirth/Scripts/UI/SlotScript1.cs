﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotScript1 : MonoBehaviour
{
    [SerializeField]
    public ExecutableSlotScript executableScript;

    public void Execute()
    {
        executableScript.Execute();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
