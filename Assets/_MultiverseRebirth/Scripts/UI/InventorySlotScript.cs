﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "UI/Slot Script/Inventory Slot Script")]
/// inheritance is require to be able to use this is SlotScript
public class InventorySlotScript : ExecutableSlotScript
{
    public override void Execute()
    {
        Debug.Log("Inv executed");
    }
}
