﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "UI/Slot Script/Base Slot Script")]
public abstract class ExecutableSlotScript : ScriptableObject
{
    public abstract void Execute();    
}
