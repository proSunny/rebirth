﻿public class ParametersList
{
    public enum Parameter
    {
        None,
        Strenght,
        Dexterity,
        Intelligence,
        Willpower,        
        Vitality,
        Luck,

        Health,
        PhysicalDamage,
        SpellDamage,
        TrueDamage,
        AllDamage,
        CritChance,
        CritMultiplier,
        PhysicalEvadeChance,
        PhysicalBlockChance,
        SpellEvadeChance,
        SpellBlockChance,
        HitChance,
        PiercingChance,
        Range,
        MovementSpeed,
        AttackSpeed
    };

    public static Parameter[] MainParameters = {
        Parameter.Strenght,
        Parameter.Dexterity,
        Parameter.Intelligence,
        Parameter.Willpower,
        Parameter.Vitality,
        Parameter.Luck,
    };

    public static Parameter[] Modificators = {
        Parameter.Health,
        Parameter.PhysicalDamage,
        Parameter.SpellDamage,
        Parameter.TrueDamage,
        Parameter.AllDamage,
        Parameter.CritChance,
        Parameter.CritMultiplier,
        Parameter.PhysicalEvadeChance,
        Parameter.PhysicalBlockChance,
        Parameter.SpellEvadeChance,
        Parameter.SpellBlockChance,
        Parameter.HitChance,
        Parameter.PiercingChance,
        Parameter.Range,
        Parameter.MovementSpeed,
        Parameter.AttackSpeed
    };

}
