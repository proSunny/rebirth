﻿using System.Collections;
using System.Collections.Generic;

public interface IParameter {

    ParametersList.Parameter ParameterName { get; set; }
    List<IAttribute> Attributes { get; set; }
    float Value { get; set; }    

    float GetValue();
}
