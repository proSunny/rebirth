﻿using System.Collections;
using System.Collections.Generic;

public interface IComponent {

    List<IAttribute> GetAttributes();

}
