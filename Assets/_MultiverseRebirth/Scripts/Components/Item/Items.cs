﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : IComponent
{
    private readonly ICharacter owner;
    public List<BaseItem> ItemsList { get; set; }
    public List<BaseItem> EquippedItemsList { get; set; }

    public Items(ICharacter owner)
    {
        this.owner = owner;
    }

    public void AddItem(BaseItem item)
    {
        ItemsList.Add(item);
    }

    public void RemoveItem(BaseItem item)
    {
        ItemsList.Remove(item);
    }

    public void EquipItem(BaseItem item)
    {
        // check if equipping is possible (race restrictions)
        EquippedItemsList.Add(item);
    }

    public void UnEquipItem(BaseItem item)
    {
        EquippedItemsList.Remove(item);
    }

    /* Return equipped items attributes */
    public List<IAttribute> GetAttributes()
    {
        throw new System.NotImplementedException();
    }
}
