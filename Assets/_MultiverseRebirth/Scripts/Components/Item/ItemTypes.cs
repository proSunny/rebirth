﻿
public enum ItemTypes
{
    None,
    MainHand,
    OffHand,
    Body,
    Head,
    Ring,
    Amulet,
    Boots,
    Consumable,
    Flask
}


public class EquipmentTypes
{
    public enum ItemType
    {        
        Head,
        Body,
        Leggings,
        Shoes,
        OneHandWeapon,
        TwoHandWeapon,
        MainHand,
        OffHand,
        Shield,
        Accessory,
        Amulet,
        Special,
        Resource,
        Consumable,
        Flask
    }

    public enum WeaponType
    {
        None,
        Axe,
        Mase,
        Sword,
        Katana,
        Castete,
        Bow,
        waterWeapon,
        airWeapom,
        fireWeapon,
        deathWeapon,
        chaosWeapon
    }
}
