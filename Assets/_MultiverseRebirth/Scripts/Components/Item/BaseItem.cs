﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseItem 
{
    public int id = 0;    
    public string name = "New Item";    
    public string itemTag = "NewItem";    
    public string description = "New Descrition";
    public string tooltip = "New Tooltip";

    public EquipmentTypes.ItemType type;
    public EquipmentTypes.WeaponType weaponType;

    public float price = 0;
    public int tier = 1;
    public int stackSize = 0;

    public ItemInfo itemInfo;
    
    public Sprite image;

    [SerializeField]
    public List<Attribute> attributes;

    public BaseItem()
    {        
        this.attributes = new List<Attribute>();
    }

    //Image Dimensions
}
