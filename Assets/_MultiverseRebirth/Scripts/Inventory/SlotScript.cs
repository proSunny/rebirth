﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class SlotScript : MonoBehaviour, IDropHandler
{
    public abstract void OnDrop(PointerEventData eventData);
    public abstract void ClearItems();
}
