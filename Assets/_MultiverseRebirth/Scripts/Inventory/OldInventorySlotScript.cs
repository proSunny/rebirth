﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OldInventorySlotScript : SlotScript, IPointerClickHandler
{
    [SerializeField]
    private GameObject itemEntity;
    private Transform currentitemEntity;

    private Image itemImage;

    [SerializeField]
    private Text stackSizeText;

    private Vector3 basePosition;

    private int itemsCount;

    public bool IsEmpty
    {
        get
        {
            return currentitemEntity?.GetComponent<ItemScript>()?.Item == null;
        }
    }

    public ItemScript SlotItemScript
    {
        get
        {
            return currentitemEntity?.GetComponent<ItemScript>();
        }
    }

    public BaseItem SlotItem
    {
        get
        {
            if (!IsEmpty)
            {
                return currentitemEntity?.GetComponent<ItemScript>()?.Item;
            }

            return null;
        }
    }

    public void Awake()
    {
        /*
        items.OnPop += new UpdateStackEvent(updateStackSize);
        items.OnPush += new UpdateStackEvent(updateStackSize);
        items.OnClear += new UpdateStackEvent(updateStackSize);
        */
    }

    public bool AddItem(BaseItem item)
    {
        if (IsEmpty)
        {
            //items.Push(item);            

            GameObject NewitemEntity = Instantiate(itemEntity, transform);

            /// Attaching item data to itemEntity
            ItemScript itemScript = NewitemEntity.GetComponent<ItemScript>();
            currentitemEntity = NewitemEntity.transform;
            itemScript.Item = item;
            itemScript.currentStackSize = 1;

            itemImage = NewitemEntity.GetComponent<Image>();
            itemImage.sprite = item.image;
            itemImage.color = Color.white;
        }
        else
        {
            // here should be check for max stackSize !!!
            //items.Push(item);
            SlotItemScript.currentStackSize++;
        }

        itemsCount = SlotItemScript.currentStackSize;

        updateStackSize();

        return true;
    }

    public bool PutItem(BaseItem item)
    {
        if (IsEmpty)
        {
            //items.Push(item);            
            SlotItemScript.currentStackSize = 1;
        }
        else
        {
            // here should be check for max stackSize !!!
            //items.Push(item);
            SlotItemScript.currentStackSize = 1;
        }

        //itemsCount = items.Count;
        itemsCount = SlotItemScript.currentStackSize;
        updateStackSize();

        return true;
    }

    public void PutItem(Transform itemEntity)
    {
        currentitemEntity = itemEntity.transform;
        itemImage = itemEntity.GetComponent<Image>();
        itemImage.sprite = itemEntity.GetComponent<Image>().sprite;
        itemImage.color = Color.white;

        itemEntity.localPosition = Vector3.zero;
    }

    public void RemoveItem()
    {
        if (!IsEmpty)
        {
            //items.Pop();
            if (IsEmpty)
            {
                Destroy(currentitemEntity.gameObject);
            }
        }
    }

    public override void ClearItems()
    {
        currentitemEntity = null;
        updateStackSize();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            // RemoveItem();
            EquipItem();
        }
        /*
        if (eventData.button == PointerEventData.InputButton.Left)
            Debug.Log("Left click");
        else if (eventData.button == PointerEventData.InputButton.Middle)
            Debug.Log("Middle click");
        else if (eventData.button == PointerEventData.InputButton.Right)
            Debug.Log("Right click");
        */
    }

    public int StackItem(BaseItem item, int stackSize = 1)
    {
        if (!IsEmpty && item.itemTag == SlotItem.itemTag)
        {
            int stackSizeSum = SlotItemScript.currentStackSize + stackSize;
            if (SlotItem.stackSize >= stackSizeSum)
            {
                SlotItemScript.currentStackSize += stackSize;
                updateStackSize();
                return 0;
            }
            else
            {
                SlotItemScript.currentStackSize = SlotItem.stackSize;
                updateStackSize();
                return stackSizeSum - SlotItem.stackSize;
            }
        }
        return -1;
    }

    public int StackItem(ItemScript itemScript)
    {
        if (!IsEmpty && itemScript.Item.itemTag == SlotItem.itemTag)
        {
            int stackSizeSum = SlotItemScript.currentStackSize + itemScript.currentStackSize;
            if (SlotItem.stackSize >= stackSizeSum)
            {
                SlotItemScript.currentStackSize = stackSizeSum;
                updateStackSize();
                return 0;
            }
            else
            {
                SlotItemScript.currentStackSize = SlotItem.stackSize;
                updateStackSize();
                return stackSizeSum - SlotItem.stackSize;
            }
        }
        return -1;
    }

    public void SwapItems(ItemScript item)
    {
        return;
    }

    public int GetItemsCount()
    {
        //return ItemScript.currentStackSize;
        return 1;
    }

    public void UseItem()
    {
        /*
        if (Item is IUsable)
        {
            (Item as IUsable).Use();
        }
        */
    }

    public void EquipItem()
    {
        if (!IsEmpty)
        {
            Debug.Log("Item equipped!" + SlotItem.name);
        }        
    }

    public void updateStackSize()
    {
        if (!IsEmpty && SlotItemScript.currentStackSize > 1)
        {
            stackSizeText.text = SlotItemScript.currentStackSize.ToString();
            stackSizeText.color = Color.white;
        }
        else
        {
            stackSizeText.color = new Color(0, 0, 0, 0);
        }
    }

    public override void OnDrop(PointerEventData eventData)
    {
        var droppedObject = eventData.pointerDrag;

        /// [WARNING] ItemScript DEPENDENCY in originalObj !!!
        /// Code below is only for item drop
        var droppedItemScript = droppedObject?.GetComponent<ItemScript>();
        if (droppedItemScript == null)
        {
            return;
        }

        if (IsEmpty)
        {
            droppedItemScript.isDropped = true;
            droppedItemScript.parentCarrier = transform;
            currentitemEntity = droppedItemScript.transform;
        }
        else
        {
            Transform droppedItemParent = droppedItemScript.parentCarrier;

            OldInventorySlotScript droppedItemSlotScript = droppedItemParent.GetComponent<OldInventorySlotScript>();
            if (droppedItemSlotScript == this)
            {
                return;
            }

            int stackResult = StackItem(droppedItemScript);
            /// stack with excess
            if (stackResult > 0)
            {
                droppedItemScript.currentStackSize = stackResult;
                droppedItemScript.isDropped = false;

                droppedItemSlotScript.updateStackSize();
            }
            /// stack without excess
            else if (stackResult == 0)
            {
                droppedItemScript.isDropped = true;
                droppedItemScript.parentCarrier = null;
            }
            /// stacking not possible, swapping positions
            else
            {
                droppedItemScript.isDropped = true;
                droppedItemScript.isSwapped = true;

                droppedItemSlotScript.ClearItems();

                currentitemEntity.SetParent(droppedItemParent);
                droppedItemSlotScript.PutItem(SlotItemScript.transform);

                droppedItemSlotScript.updateStackSize();

                droppedItemScript.parentCarrier = transform;
                currentitemEntity = droppedItemScript.transform;
            }
        }

        updateStackSize();
        /// set itemScript.parent = null; to destroy item after drop
        // itemScript.parent = null;
        //PutItem(itemScript.Item);
    }
}