﻿internal interface IUsable
{
    void Use();
}