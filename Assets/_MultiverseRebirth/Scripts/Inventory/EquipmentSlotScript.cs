﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EquipmentSlotScript : SlotScript
{

    [SerializeField]
    private EquipmentTypes.ItemType itemType;

    [SerializeField]
    private GameObject equipmentEntity;
    private Transform currentitemEntity;

    private Image itemImage;

    public UnityEvent OnItemChanged;

    public bool IsEmpty
    {
        get
        {
            return currentitemEntity?.GetComponent<ItemScript>()?.Item == null;
        }
    }

    public ItemScript SlotItemScript
    {
        get
        {
            return currentitemEntity?.GetComponent<ItemScript>();            
        }
    }

    public BaseItem SlotItem
    {
        get
        {
            if (!IsEmpty)
            {
                return currentitemEntity?.GetComponent<ItemScript>()?.Item;
            }

            return null;
        }
    }

    public void Awake()
    {
        /*
        items.OnPop += new UpdateStackEvent(updateStackSize);
        items.OnPush += new UpdateStackEvent(updateStackSize);
        items.OnClear += new UpdateStackEvent(updateStackSize);
        */
    }

    public void PutItem(Transform itemEntity)
    {
        currentitemEntity = itemEntity.transform;
        itemImage = itemEntity.GetComponent<Image>();
        itemImage.sprite = itemEntity.GetComponent<Image>().sprite;
        itemImage.color = Color.white;

        itemEntity.localPosition = Vector3.zero;
    }

    public void RemoveItem()
    {
        if (!IsEmpty)
        {
            //items.Pop();
            if (IsEmpty)
            {
                Destroy(currentitemEntity.gameObject);
            }
        }
    }

    public override void ClearItems()
    {
        currentitemEntity = null;
    }

    public override void OnDrop(PointerEventData eventData)
    {        
        var droppedObject = eventData.pointerDrag;

        /// [WARNING] ItemScript DEPENDENCY in originalObj !!!
        /// Code below is only for item drop
        var droppedItemScript = droppedObject?.GetComponent<ItemScript>();
        if (droppedItemScript == null)
        {
            return;
        }

        if (IsEmpty)
        {
            if (droppedItemScript.Item.type == itemType)
            {
                OnItemChanged.Invoke();

                droppedItemScript.isDropped = true;
                droppedItemScript.parentCarrier = transform;
                currentitemEntity = droppedItemScript.transform;
                Debug.Log("Item droppped");
            }
            
        } else
        {
            Transform droppedItemParent = droppedItemScript.parentCarrier;

            OldInventorySlotScript droppedItemCellScript = droppedItemParent.GetComponent<OldInventorySlotScript>();
            if (droppedItemCellScript == this)
            {
                return;
            }

            droppedItemScript.isDropped = true;        
            droppedItemScript.isSwapped = true;
                
            droppedItemCellScript.ClearItems();                

            currentitemEntity.SetParent(droppedItemParent);
            droppedItemCellScript.PutItem(SlotItemScript.transform);

            droppedItemCellScript.updateStackSize();

            droppedItemScript.parentCarrier = transform;
            currentitemEntity = droppedItemScript.transform;

            Debug.Log("Item NOOOT droppped");
        }

        /// set itemScript.parent = null; to destroy item after drop
        // itemScript.parent = null;
        //PutItem(itemScript.Item);
    }
}
