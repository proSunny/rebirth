﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ItemDragHandler : DragHandler
{
    private GameObject item; //item being dragged
    private ItemScript itemScript; //item being dragged
    private SlotScript startSlotScript; //item being dragged

    private Vector3 startPosition;
    private Transform startParent;

    public override void OnBeforeDrag()
    {
        item = gameObject;
        itemScript = gameObject?.GetComponent<ItemScript>();
        if (itemScript == null)
        {
            return;
        }
        startPosition = transform.position;
        startParent = transform.parent;
     
        itemScript.parentCarrier = startParent;
        /// <summary>
        /// [WARNING] parent.SlotScript DEPENDENCY in parent !!!
        /// </summary>
        startSlotScript = startParent.GetComponent<SlotScript>();
        //itemScript.currentStackSize = startInventorySlotScript.GetItemsCount();
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        if (itemScript.isDropped)
        {
            if (itemScript.parentCarrier != null)
            {
                transform.SetParent(itemScript.parentCarrier);
                transform.localPosition = Vector3.zero;
                /// because OnEndDrag happends after OnDrop we don't have to clear startInventorySlotScript.currentItemEntity. 
                if (!itemScript.isSwapped)
                {
                    startSlotScript.ClearItems();
                }                
            } else
            {
                /// if dropping is possible but parrent is not received, this should be destoyed
                startSlotScript.ClearItems();
                /// order is important!
                Destroy(this.gameObject);
            }
            
        } else
        {
            transform.position = startPosition;
            transform.SetParent(startParent);
        }

        itemScript.isSwapped = false;
        itemScript.isDropped = false;

        if (draggingIcon != null)
            Destroy(draggingIcon);

        draggingIcon = null;
    }
}