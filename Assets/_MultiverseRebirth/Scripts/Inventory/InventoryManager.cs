﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public GameObject inventorySlot; // This is our prefab object that will be exposed in the inspector

    public int inventorySlotCount; // number of objects to create. Exposed in inspector

    private ItemsData itemsData;
    private List<OldInventorySlotScript> slots = new List<OldInventorySlotScript>();

    private Dictionary<EqipmentType, BaseItem> equippedItems = new Dictionary<EqipmentType, BaseItem>
    {
        { EqipmentType.Head, null },
        { EqipmentType.Body, null },
        { EqipmentType.LeftWeapon, null },
        { EqipmentType.RightWeapon, null },
        { EqipmentType.Leggings, null },
        { EqipmentType.Shoes, null },
        { EqipmentType.Accessory1, null },
        { EqipmentType.Accessory2, null },
        { EqipmentType.Accessory3, null },
        { EqipmentType.Accessory4, null }
    };

    private Dictionary<EquipmentTypes.ItemType, EqipmentType> itemToEqipmentTypes = new Dictionary<EquipmentTypes.ItemType, EqipmentType> {
        { EquipmentTypes.ItemType.Head, EqipmentType.Head },
        { EquipmentTypes.ItemType.Body, EqipmentType.Body },
        { EquipmentTypes.ItemType.Leggings, EqipmentType.Leggings },
        { EquipmentTypes.ItemType.Shoes, EqipmentType.Shoes },
    };

    public enum EqipmentType
    {
        None,
        Head,
        Body,
        Leggings,
        Shoes,
        Weapon,
        LeftWeapon,
        RightWeapon,        
        Accessory,        
        Accessory1,        
        Accessory2,        
        Accessory3,
        Accessory4
    }
    /*

  OnEquip
  OnUnequip
  OnSell
  OnBuy
  OnLoot
  OnDrop

  */

    /*
     only equipped:
        compare

    all items
        buy/sell
        save/load
        shop UI
        loot
     
     
     
     */

    void Start()
    {
        itemsData = AssetDatabase.LoadAssetAtPath("Assets/Items/ItemsData.asset", typeof(ItemsData)) as ItemsData;
        // Debug.Log(itemsData.ItemList);

        Populate();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            // BaseItem item = new BaseItem();
            // item.image = 
            AddItem(itemsData.ItemList[0]);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            // BaseItem item = new BaseItem();
            // item.image = 
            AddItem(itemsData.ItemList[1]);
        }
    }

    public void AddItem(BaseItem item)
    {
        if (item.stackSize > 0)
        {
            if (AddItemInStack(item))
            {
                return;
            }
        }
        AddItemInEmpty(item);
    }

    
    public void AddItemInEmpty(BaseItem item)
    {        
        foreach (OldInventorySlotScript slot in slots)
        {
            if (slot.IsEmpty)
            {
                slot.AddItem(item);
                return;
            }
        }
    }

    
    public bool AddItemInStack(BaseItem item)
    {        
        foreach (OldInventorySlotScript slot in slots)
        {                        
            if (slot.StackItem(item) == 0)
            {
                return true;
            }            
        }
        return false;
    }

    void Populate()
    {

        // GameObject newObj; // Create GameObject instance

        for (int i = 0; i < inventorySlotCount; i++)
        {
            // Create new instances of our prefab until we've created as many as we specified
            OldInventorySlotScript cell = Instantiate(inventorySlot, transform).GetComponent<OldInventorySlotScript>();
            cell.name += i;
            slots.Add(cell);            

            // Randomize the color of our image
            // newObj.GetComponent<Image>().color = Random.ColorHSV();
        }

        // inventoryCell.SetActive(false);
    }

    private EqipmentType GetEquipmentSlotType(EquipmentTypes.ItemType itemType)
    {
        if (itemToEqipmentTypes.ContainsKey(itemType))
        {
            return itemToEqipmentTypes[itemType];
        }
        if (itemType == EquipmentTypes.ItemType.OneHandWeapon || itemType == EquipmentTypes.ItemType.TwoHandWeapon || itemType == EquipmentTypes.ItemType.Shield)
        {
            return EqipmentType.Weapon;
        }
        if (itemType == EquipmentTypes.ItemType.Accessory)
        {
            return EqipmentType.Accessory;
        }

        return EqipmentType.None;
    }
    /*
    public BaseItem EquipItem(BaseItem item)
    {
        ItemTypes.ItemType itemType = item.type;

        EqipmentType eqipmentType = GetEquipmentSlotType(itemType);

        if (eqipmentType == EqipmentType.Weapon)
        {
            // bla bla
        } else if (eqipmentType == EqipmentType.Accessory)
        {
            // bla bla bla
        }
        else 
        {
            equippedItems[eqipmentType] = item;
        }

        if (equippedItems[itemType] == null)
        {
            equippedItems[itemType] = item;
            return null;
        } else
        {
            BaseItem eqippedItem = equippedItems[itemType];
            equippedItems[itemType] = item;
            return eqippedItem;
        }        
    }

    public BaseItem UnqeuipItem(ItemTypes.ItemType itemType)
    {
        BaseItem eqippedItem = equippedItems[itemType];
        return eqippedItem;
    }*/
}
