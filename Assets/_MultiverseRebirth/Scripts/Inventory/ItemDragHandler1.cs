﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ItemDragHandler1 : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject item; //item being dragged
    public static ItemScript itemScript; //item being dragged
    public static SlotScript startInventoryCellScript; //item being dragged

    public UnityEvent DragOverObjectEvent;

    private Vector3 startPosition;
    private Transform startParent;

    public void OnBeginDrag(PointerEventData eventData)
    {
        item = gameObject;
        itemScript = gameObject?.GetComponent<ItemScript>();
        if (itemScript == null)
        {
            return;
        }

        startPosition = transform.position;
        startParent = transform.parent;
        
        /// warning parent.InventoryCellScript DEPENDENCY in parent !!!
        startInventoryCellScript = startParent.GetComponent<SlotScript>();
        //itemScript.currentStackSize = startInventoryCellScript.GetItemsCount();

        itemScript.parentCarrier = startParent;

        GetComponent<CanvasGroup>().blocksRaycasts = false;

        transform.SetParent(transform.root);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (itemScript.isDropped)
        {
            if (itemScript.parentCarrier != null)
            {
                transform.SetParent(itemScript.parentCarrier);
                transform.localPosition = Vector3.zero;
                /// because OnEndDrag happends after OnDrop we don't have to clear startInventoryCellScript.currentItemEntity. 
                if (!itemScript.isSwapped)
                {
                    startInventoryCellScript.ClearItems();
                }                
            } else
            {
                /// if dropping is possible but parrent is not received, this should be destoyed
                startInventoryCellScript.ClearItems();
                /// order is important!
                Destroy(this.gameObject);
            }
            
        } else
        {
            transform.position = startPosition;
            transform.SetParent(startParent);
        }

        itemScript.isSwapped = false;
        itemScript.isDropped = false;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    static public T FindInParents<T>(GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null)
            return comp;

        var t = go.transform.parent;
        while (t != null && comp == null)
        {
            comp = t.gameObject.GetComponent<T>();
            t = t.parent;
        }
        return comp;
    }
}