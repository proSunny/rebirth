﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ItemScript : MonoBehaviour
{
    [HideInInspector]
    public bool isDropped, isSwapped = false;

    
    /// This variable only for passing parent from one script to another! 
    /// Don't use it as a refference to current GO parent!
    public Transform parentCarrier;
    public int currentStackSize;

    private BaseItem item;
    public BaseItem Item { get => item; set => item = value; }
}
