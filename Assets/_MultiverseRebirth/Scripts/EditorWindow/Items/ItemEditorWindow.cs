﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Reflection;

public class ItemEditorWindow : EditorWindow
{
    public ItemsData itemsData;

    new string name = "New Item";
    string description = "New description";
    float price;
    int tier;
    Sprite icon;
    AttributeList attributes;
    Attributes attributesList;

    Texture deleteButtonTexture;

    
    Vector2 scrollPos;
    Vector2 attributesScrollPos;

    private BaseItem _currentInventoryItem;


    [MenuItem("Game/Item Data Editor Window")]
    static void Init()
    {        
        EditorWindow.GetWindow(typeof(ItemEditorWindow));        
    }

    void OnEnable()
    {
        _currentInventoryItem = null;
        deleteButtonTexture = Resources.Load("Textures/icons/trashBin") as Texture;
        Debug.Log(deleteButtonTexture);

        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            itemsData = AssetDatabase.LoadAssetAtPath(objectPath, typeof(ItemsData)) as ItemsData;
        }
    }

    void OnGUI()
    {        
        GUILayout.BeginHorizontal();       

        GUILayout.Label("Item Manager", EditorStyles.boldLabel);

        if (GUILayout.Button("Add new Item"))
        {
            AddItem(name, description);
        }
        if (GUILayout.Button("Open Item List"))
        {
            OpenItemList();
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(20);

        GUILayout.BeginArea(new Rect(0, 40, Screen.width / 3, Screen.height));

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(300), GUILayout.Height(Screen.height - 20));

        if(itemsData != null && itemsData.ItemList != null)
        {                        
            foreach (BaseItem item in itemsData.ItemList)
            {
                GUILayout.BeginHorizontal();
                if (item.image)
                {
                    GUILayout.Box(item.image.texture, GUILayout.Width(50), GUILayout.Height(50));
                }

                GUILayout.BeginVertical();
                GUILayout.Space(15);

                if (GUILayout.Button(item.name, GUILayout.Width(200), GUILayout.Height(30)))
                {
                    GUI.FocusControl(null);
                    _currentInventoryItem = item;
                }

                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
            }
        }

        EditorGUILayout.EndScrollView();
        GUILayout.EndArea();

        GUILayout.BeginArea(new Rect(Screen.width / 3 + 0, 50, Screen.width / 2, Screen.height));
        
        if (_currentInventoryItem != null)
        {
            GUILayout.Label("Item Information", EditorStyles.boldLabel);

            DisplayItemInfo(_currentInventoryItem);
        }
        
        GUILayout.EndArea();

        EditorUtility.SetDirty(itemsData);
    }

    void OpenItemList()
    {
        string absPath = EditorUtility.OpenFilePanel("Select Inventory Item List", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            itemsData = AssetDatabase.LoadAssetAtPath(relPath, typeof(ItemsData)) as ItemsData;
            Debug.Log(itemsData);

            if (itemsData)
            {
                EditorPrefs.SetString("ObjectPath", relPath);
            }
        }
    }

    void DisplayItemInfo(BaseItem item)
    {
        GUILayout.Label("ID: " + item.id, EditorStyles.boldLabel);
        item.name = EditorGUILayout.TextField("Name", item.name);
        item.itemTag = EditorGUILayout.TextField("Tag", item.itemTag);
        item.description = EditorGUILayout.TextField("Description", item.description);
        item.tooltip = EditorGUILayout.TextField("ToolTip", item.tooltip);
        item.type = (EquipmentTypes.ItemType)EditorGUILayout.EnumPopup("Item Type", item.type);
        item.weaponType = (EquipmentTypes.WeaponType)EditorGUILayout.EnumPopup("Item Weapon Type", item.weaponType);
        item.price = EditorGUILayout.FloatField("Price", item.price);
        item.tier = EditorGUILayout.IntField("Tier", item.tier);
        item.stackSize = EditorGUILayout.IntField("Stack Size", item.stackSize);
        item.image = (Sprite)EditorGUILayout.ObjectField("Icon" ,item.image, typeof(Sprite), false);        

        GUILayout.Label("Item Attrributes", EditorStyles.boldLabel);
        if (GUILayout.Button("Add new Attribute", GUILayout.Width(150), GUILayout.Height(20)))
        {
            Attribute newAttribute = new Attribute
            {
                parameter = ParametersList.Parameter.Strenght,
                value = 5,
                valueMod = AttributeType.ValueMod.exact
            };
            item.attributes.Add(newAttribute);
        }

        if (item.attributes != null)
        {            
            GUILayout.BeginHorizontal();
            DisplayAttributesInfo(item.attributes);
            GUILayout.EndHorizontal();
        }                

        if (GUILayout.Button("Remove Item"))
        {
            bool check = EditorUtility.DisplayDialog("Remove Item", "Are you sure you want to remove this item?", "Yes", "No");

            if (check)
            {
                itemsData.ItemList.Remove(item);
                _currentInventoryItem = null;
            }
        }
    }

    void DisplayAttributesInfo(List<Attribute> attributes)
    {        
        GUILayout.BeginVertical();
        GUILayout.Space(15);
        attributesScrollPos = EditorGUILayout.BeginScrollView(attributesScrollPos, GUILayout.Width(400), GUILayout.Height(Screen.height - 400));
        /* using for loop to avoid enumerates created by foreach loop. Enumerates can't be modified during access */
        for (int i = 0; i < attributes.Count; i++)
        {            
            attributes[i].parameter = (ParametersList.Parameter)EditorGUILayout.EnumPopup("Attributes", attributes[i].parameter);
            attributes[i].value = EditorGUILayout.FloatField("Value", attributes[i].value);
            attributes[i].valueMod = (AttributeType.ValueMod)EditorGUILayout.EnumPopup("Attributes", attributes[i].valueMod);
            var oldColor = GUI.backgroundColor;
            //GUI.backgroundColor = Color.red;

            GUILayout.BeginHorizontal();
            GUILayout.Space(250);
            if (GUILayout.Button("Remove attribute", GUILayout.Width(120), GUILayout.Height(20)))
            {
                bool check = EditorUtility.DisplayDialog("Remove Attribute", "Are you sure you want to remove this attribute?", "Yes", "No");

                if (check)
                {
                    attributes.Remove(attributes[i]);                    
                }
            }
            GUILayout.EndHorizontal();

            GUI.backgroundColor = oldColor;
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);            
        }
        EditorGUILayout.EndScrollView();
        GUILayout.EndVertical();
    }

    void AddItem(string name, string description)
    {
        _currentInventoryItem = null;
        int id = 0;

        if (itemsData.ItemList == null)
        {
            itemsData.ItemList = new List<BaseItem>();
        } else
        {
            int listCount = itemsData.ItemList.Count;
            id = itemsData.ItemList[(listCount-1)].id + 1;
        }

        BaseItem newItem = new BaseItem
        {            
            id = id,
            name = name,
            description = description,
            attributes = new List<Attribute>(),
        };
       
        itemsData.ItemList.Add(newItem);
    }
}
