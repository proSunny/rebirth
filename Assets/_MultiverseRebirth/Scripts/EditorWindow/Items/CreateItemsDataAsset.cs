﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateItemsDataAsset : MonoBehaviour
{
    [MenuItem("Assets/Inventory/Items Data")]
    public static ItemsData Create()
    {
        ItemsData asset = ScriptableObject.CreateInstance<ItemsData>();

        AssetDatabase.CreateAsset(asset, "Assets/Items/ItemsData.asset");
        AssetDatabase.SaveAssets();
         
        return asset;
    }
}
