﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsData : ScriptableObject
{
    /* Hiden from inspector to not be able change "size" of the array in inspector */
    // [HideInInspector]
    public List<BaseItem> ItemList;
}

