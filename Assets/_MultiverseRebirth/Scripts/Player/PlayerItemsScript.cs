﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Player/Items")]
public class PlayerItemsScript : ScriptableObject
{
    public List<BaseItem> items;    

 
}
