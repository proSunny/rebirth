﻿using System.Collections;
using System.Collections.Generic;

public interface ICharacter {

    Parameters Parameters { get; set; }
    List<IComponent> Components { get; set; }
}
