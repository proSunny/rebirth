﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, ICharacter
{
    public Parameters Parameters { get; set; }
    public List<IComponent> Components { get; set; }
    /*
    
    allItems
    equipedItems
         
    */

    public PlayerStatsScript playerStats;

    public Items Items { get; set; }

    public Player()
    {
        
    }
}
