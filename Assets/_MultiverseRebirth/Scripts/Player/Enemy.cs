﻿using System.Collections;
using System.Collections.Generic;

class Enemy : ICharacter {

    public Parameters Parameters { get; set; }
    public List<IComponent> Components { get; set; } // Items, OwnerEffects, Bonuses
}
