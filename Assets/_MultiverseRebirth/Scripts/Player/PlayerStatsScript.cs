﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Player/Stats")]
public class PlayerStatsScript : SerializedScriptableObject
{
    [SerializeField]
    private Dictionary<ParametersList.Parameter, PlayerStat> stats = new Dictionary<ParametersList.Parameter, PlayerStat>();    

    public float this[ParametersList.Parameter key]
    {
        // returns value if exists
        get { return stats[key].Value; }

        // actually, doen't need setter here !
        // updates if exists, adds if doesn't exist
        // set { playerStats[key].AddModifier(new StatModifier(value, StatModType.Flat)); }
    }

    public void OnEnable()
    {
        stats.Add(ParametersList.Parameter.Strenght, new PlayerStat(1));
        stats.Add(ParametersList.Parameter.Dexterity, new PlayerStat(1));
        stats.Add(ParametersList.Parameter.Intelligence, new PlayerStat(1));
        stats.Add(ParametersList.Parameter.Willpower, new PlayerStat(1));
        stats.Add(ParametersList.Parameter.Luck, new PlayerStat(1));
        stats.Add(ParametersList.Parameter.Vitality, new PlayerStat(1));
    }

    public void UpdateStat(ParametersList.Parameter parameter, float value)
    {
        stats[parameter].AddModifier(new StatModifier(value, StatModType.Flat));
    }

    public void UpdateStat(ParametersList.Parameter parameter, float value, object source)
    {
        stats[parameter].AddModifier(new StatModifier(value, StatModType.Flat, source));
    }

    public void UpdateStat(ParametersList.Parameter parameter, float value, StatModType modType, object source)
    {
        stats[parameter].AddModifier(new StatModifier(value, modType, source));
    }

    public void RemoveStat(ParametersList.Parameter parameter, object source)
    {
        stats[parameter].RemoveAllModifiersFromSource(source);
    }
}
