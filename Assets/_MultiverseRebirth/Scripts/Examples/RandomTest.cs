﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTest : MonoBehaviour
{
    //Variable to store current weapontype
    private ClassWeapon.WeaponType currentWeaponType;

    //Call this from anywhere to fill in the variable 
    public void AddWeapon(ClassWeapon.WeaponType weaponType)
    {
        currentWeaponType = weaponType;
    }

    //Call this from anywhere to fill in the variable 
    public void AddWeapon(ClassWeapon weapon)
    {
        currentWeaponType = weapon.weaponType;
    }

    //Check what weapon is the currentweapon and execute code from there
    public void DoDamage()
    {
        //Check if currenweapon is exually filled to be less bug heavy
        if (currentWeaponType != ClassWeapon.WeaponType.none)
        {
            if (currentWeaponType == ClassWeapon.WeaponType.sword)
            {
                Debug.Log("sword attack");
            }
            else if (currentWeaponType == ClassWeapon.WeaponType.axe)
            {
                Debug.Log("axe attack");
            }
            else if (currentWeaponType == ClassWeapon.WeaponType.bow)
            {
                Debug.Log("bow attack");
            }
        }
    }
}

//Weapons you can choose from
public class ClassWeapon
{
    public enum WeaponType
    {
        none, sword, axe, bow
    }
    public WeaponType weaponType;
}