﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "UI/Flexible UI Text Button")]
public class FlexibleUITextButtonData : FlexibleUIData
{
    [SerializeField]
    private Sprite buttonSprite;
    public override Sprite ButtonSprite { get => buttonSprite; set => buttonSprite = value; }

    [SerializeField]
    private SpriteState buttonSpriteState;
    public override SpriteState ButtonSpriteState { get => buttonSpriteState; set => buttonSpriteState = value; }

    [SerializeField]
    private Color defaultTextColor;
    public override Color DefaultTextColor { get => defaultTextColor; set => defaultTextColor = value; }

    [SerializeField]
    private Color hoveredTextColor;
    public override Color HoveredTextColor { get => hoveredTextColor; set => hoveredTextColor = value; }

    [SerializeField]
    private Color pressedTextColor;
    public override Color PressedTextColor { get => pressedTextColor; set => pressedTextColor = value; }

    [SerializeField]
    private Color activatedTextColor;
    public override Color ActivatedTextColor { get => activatedTextColor; set => activatedTextColor = value; }

    [SerializeField]
    private Sprite activatedSprite;
    public override Sprite ActivatedSprite { get => activatedSprite; set => activatedSprite = value; }

    [SerializeField]
    private Color disabledTextColor;
    public override Color DisabledTextColor { get => disabledTextColor; set => disabledTextColor = value; }
}
