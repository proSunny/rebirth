﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
[RequireComponent(typeof(Button))]
[RequireComponent(typeof(Image))]
public class FlexibleUITextButton : FlexibleUI,
IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    Image image;
    [SerializeField]
    Image icon;
    Button button;

    bool inBounds;
    bool inActivated;

    Navigation customNav = new Navigation();

    public TextMeshProUGUI textGUI;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

        image = GetComponent<Image>();
        button = GetComponent<Button>();

        button.transition = Selectable.Transition.SpriteSwap;
        button.targetGraphic = image;
        customNav.mode = Navigation.Mode.None;
        button.navigation = customNav;

        
        image.type = Image.Type.Sliced;
        button.spriteState = skinData.ButtonSpriteState;

        if (!inBounds && !inActivated)
        {
            image.sprite = skinData.ButtonSprite;
            textGUI.color = skinData.DefaultTextColor;
        }        
    }

    public void ActivateButton()
    {
        textGUI.color = skinData.ActivatedTextColor;
        image.sprite = skinData.ActivatedSprite;
        inActivated = true;
    }

    public void DeActivateButton()
    {
        textGUI.color = skinData.DefaultTextColor;
        image.sprite = skinData.ButtonSprite;
        inActivated = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!inActivated)
        {
            inBounds = true;
            textGUI.color = skinData.HoveredTextColor;
        }        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!inActivated)
        {
            inBounds = false;
            textGUI.color = skinData.DefaultTextColor;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //tracking = true;
        //inBounds = true;
        textGUI.color = skinData.PressedTextColor;
        // image.sprite = skinData.PressedTextColor;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //if (tracking && inBounds && OnClick != null) OnClick.Invoke();
        //tracking = false;
        //inBounds = false;
        //UpdateStyle();
        textGUI.color = skinData.ActivatedTextColor;
        image.sprite = skinData.ActivatedSprite;
    }
}
