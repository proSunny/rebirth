﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public abstract class FlexibleUIData : ScriptableObject
{
    virtual public Sprite ButtonSprite { get; set; }
    virtual public SpriteState ButtonSpriteState { get; set; }

    virtual public Color DefaultColor { get; set; }
    virtual public Sprite DefaultIcon { get; set; }

    virtual public Color ConfirmColor { get; set; }
    virtual public Sprite ConfirmIcon { get; set; }

    virtual public Color DeclinedColor { get; set; }
    virtual public Sprite DeclinedIcon { get; set; }

    virtual public Color WarningColor { get; set; }
    virtual public Sprite WarningIcon { get; set; }

    virtual public Color DefaultTextColor { get; set; }
    virtual public Color HoveredTextColor { get; set; }
    virtual public Color PressedTextColor { get; set; }
    virtual public Color ActivatedTextColor { get; set; }
    virtual public Sprite ActivatedSprite { get; set; }
    virtual public Color DisabledTextColor { get; set; }    
}
