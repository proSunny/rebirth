﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(Image))]
public class FlexibleUIButton : FlexibleUI
{
    public enum ButtonType
    {
        Default, Confirm, Declined, Warning
    }

    Image image;
    [SerializeField]
    Button button;
    // public Image icon;
    public ButtonType buttonType;

    private FlexibleUIDataBase currentSkinData;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

        image = GetComponent<Image>();
        button = GetComponent<Button>();

        button.transition = Selectable.Transition.SpriteSwap;
        button.targetGraphic = image;

        image.sprite = skinData.ButtonSprite;
        image.type = Image.Type.Sliced;
        button.spriteState = skinData.ButtonSpriteState;
        
        switch (buttonType)
        {
            case ButtonType.Confirm:
                image.color = skinData.ConfirmColor;
                //icon.sprite = skinData.confirmIcon;
                break;
            case ButtonType.Declined:
                image.color = skinData.DeclinedColor;
                //icon.sprite = skinData.declinedIcon;
                break;
            case ButtonType.Warning:
                image.color = skinData.WarningColor;
                //icon.sprite = skinData.warningIcon;
                break;
            case ButtonType.Default:
                image.color = skinData.DefaultColor;
                //icon.sprite = skinData.defaultIcon;
                break;
        }
        
    }
}
