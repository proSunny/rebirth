﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "UI/Flexible UI Data")]
public class FlexibleUIDataBase : FlexibleUIData
{
    [SerializeField]
    private Sprite buttonSprite;
    public override Sprite ButtonSprite { get => buttonSprite; set => buttonSprite = value; }

    [SerializeField]
    private SpriteState buttonSpriteState;
    public override SpriteState ButtonSpriteState { get => buttonSpriteState; set => buttonSpriteState = value; }

    [SerializeField]
    private Color defaultColor;
    public override Color DefaultColor { get => defaultColor; set => defaultColor = value; }

    [SerializeField]
    private Color confirmColor;
    public override Color ConfirmColor { get => confirmColor; set => confirmColor = value; }

    [SerializeField]
    private Color declinedColor;
    public override Color DeclinedColor { get => declinedColor; set => declinedColor = value; }

    [SerializeField]
    private Color warningColor;
    public override Color WarningColor { get => warningColor; set => warningColor = value; }

}
