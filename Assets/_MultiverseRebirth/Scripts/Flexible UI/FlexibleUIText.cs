﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
public class FlexibleUIText : FlexibleUI,
IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    bool inBounds;
    bool inActivated;

    public TextMeshProUGUI textGUI;

    protected override void OnSkinUI()
    {
        base.OnSkinUI();

        if (!inBounds && !inActivated)
        {
            textGUI.color = skinData.DefaultTextColor;
        }        
    }

    public void ActivateButton()
    {
        textGUI.color = skinData.ActivatedTextColor;
        inActivated = true;
    }

    public void DeActivateButton()
    {
        textGUI.color = skinData.DefaultTextColor;
        inActivated = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!inActivated)
        {
            inBounds = true;
            textGUI.color = skinData.HoveredTextColor;
        }        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!inActivated)
        {
            inBounds = false;
            textGUI.color = skinData.DefaultTextColor;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //tracking = true;
        //inBounds = true;
        textGUI.color = skinData.PressedTextColor;
        // image.sprite = skinData.PressedTextColor;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //if (tracking && inBounds && OnClick != null) OnClick.Invoke();
        //tracking = false;
        //inBounds = false;
        //UpdateStyle();
        textGUI.color = skinData.ActivatedTextColor;
    }
}
