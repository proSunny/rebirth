﻿using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public MenuRuntimeSet Set;

    public void EnableMenuItem(GameObject menuItem)
    {
        for (int i = Set.Items.Count - 1; i >= 0; i--)
        {
            Set.Items[i].gameObject.SetActive(false);
        }

        menuItem.SetActive(true);

        if (Set.activeButton != null)
        {
            Set.activeButton.DeActivateButton();
            Set.activeButton = GetComponent<FlexibleUITextButton>();
        } else
        {            
            Set.activeButton = GetComponent<FlexibleUITextButton>();
        }
        if (Set.activeButton != null)
        {
            Set.activeButton.ActivateButton();
        }        
    }

    public void DisableAll()
    {
        // Loop backwards since the list may change when disabling
        for (int i = Set.Items.Count-1; i >= 0; i--)
        {
            Set.Items[i].gameObject.SetActive(false);
        }
    }

    public void DisableRandom()
    {
        int index = Random.Range(0, Set.Items.Count);
        Set.Items[index].gameObject.SetActive(false);
    }
}
