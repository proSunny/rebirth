﻿using UnityEngine;


[CreateAssetMenu(menuName = "UI/Menu Runtime Set")]
public class MenuRuntimeSet : RuntimeSet<RuntimeMenuItem>
{
    public FlexibleUITextButton activeButton;
}
