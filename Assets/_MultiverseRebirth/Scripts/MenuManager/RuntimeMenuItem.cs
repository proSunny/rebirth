﻿using UnityEngine;


public class RuntimeMenuItem : MonoBehaviour
{
    public MenuRuntimeSet RuntimeSet;

    private void OnEnable()
    {
        RuntimeSet.Add(this);
    }

    private void OnDisable()
    {
        RuntimeSet.Remove(this);
    }
}
