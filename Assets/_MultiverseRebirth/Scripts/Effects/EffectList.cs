﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectList : ScriptableObject
{
    public List<Effect> Effects;
}
