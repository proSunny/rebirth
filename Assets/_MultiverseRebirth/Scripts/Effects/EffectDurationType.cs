﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectDurationType
{
    public enum DurationType
    {
        Permanent,
        Time
    };
}
