﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Effect
{
    public string Name;
    public AttributeList.Attributes Attributes;
    public Sprite Image;
    public EffectDurationType.DurationType DurationType;
    public float DurationTime;
}
