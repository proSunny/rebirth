﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateEffect : MonoBehaviour
{
    [MenuItem("Assets/Effect/EffectList")]
    public static EffectList Create()
    {
        EffectList asset = ScriptableObject.CreateInstance<EffectList>();

        AssetDatabase.CreateAsset(asset, "Assets/Items/EffectList.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }
}
