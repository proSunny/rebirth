﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseSkill
{

    public string name = "New Item";
    public string itemTag = "NewItem";
    public string description = "New Descrition";
    public string tooltip = "New Tooltip";

    // public Effect effect;
}
